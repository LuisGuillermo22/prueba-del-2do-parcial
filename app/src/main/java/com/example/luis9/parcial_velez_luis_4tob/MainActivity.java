package com.example.luis9.parcial_velez_luis_4tob;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.luis9.parcial_velez_luis_4tob.Adapter.AdaptadorEstudiante;
import com.example.luis9.parcial_velez_luis_4tob.Models.Estudiantes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private static final String URL_Estudiantes = "http://10.1.15.127:3004/api/estudiantes";
    private RecyclerView.LayoutManager layoutManager;
    private AdaptadorEstudiante adaptadorEstudiante;
    private ArrayList<Estudiantes> arrayListP;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView)findViewById(R.id.Estudiantes);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        arrayListP = new ArrayList<>();
        adaptadorEstudiante = new AdaptadorEstudiante(arrayListP);
        progressDialog = new ProgressDialog(this);

        ObtenerEstudiantes();
    }
    private void ObtenerEstudiantes() {
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_Estudiantes, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response);
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObject =jsonArray.getJSONObject(i);
                        Estudiantes estudiantes = new Estudiantes();
                        estudiantes.setImagen(jsonObject.getString("imagen"));
                        estudiantes.setId(jsonObject.getString("id"));

                        estudiantes.setNombres(jsonObject.getString("nombres"));
                        estudiantes.setApellidos(jsonObject.getString("apellidos"));
                        estudiantes.setParcial_uno(jsonObject.getString("parcial_uno"));
                        estudiantes.setParcial_dos(jsonObject.getString("parcial_dos"));

                        arrayListP.add(estudiantes);
                    }
                    recyclerView.setAdapter(adaptadorEstudiante);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Ocurrio un error mientras se cargaba", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}