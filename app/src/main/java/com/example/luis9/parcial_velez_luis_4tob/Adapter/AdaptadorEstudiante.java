package com.example.luis9.parcial_velez_luis_4tob.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.luis9.parcial_velez_luis_4tob.Models.Estudiantes;
import com.example.luis9.parcial_velez_luis_4tob.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class AdaptadorEstudiante extends RecyclerView.Adapter<AdaptadorEstudiante.MyViewHolder>  {

    private ArrayList<Estudiantes> hola;

    public AdaptadorEstudiante(ArrayList<Estudiantes> hola){ this.hola = hola; }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_detalle, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        Estudiantes hola1 = hola.get(position);
        myViewHolder.id.setText(hola1.getId());
        myViewHolder.Nombre.setText(hola1.getNombres());
        myViewHolder.Apellido.setText(hola1.getApellidos());
        myViewHolder.parcialuno.setText(hola1.getParcial_uno());
        myViewHolder.parcialdos.setText(hola1.getParcial_dos());

        Picasso.get().load(hola1.getImagen()).into(myViewHolder.imagen);
    }

    @Override
    public int getItemCount() { return hola.size(); }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imagen;
        private TextView id, Nombre, Apellido, parcialuno, parcialdos;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imagen = (ImageView)itemView.findViewById(R.id.Imagen);
            id = (TextView)itemView.findViewById(R.id.id);
            parcialuno = (TextView)itemView.findViewById(R.id.parcialuno);
            parcialdos = (TextView)itemView.findViewById(R.id.parcialdos);
            Nombre = (TextView)itemView.findViewById(R.id.nombre);
            Apellido = (TextView)itemView.findViewById(R.id.apellido);


        }
    }
}
